package com.sasey.nodemcucontroller;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.videolan.libvlc.LibVLC;
import org.videolan.libvlc.Media;
import org.videolan.libvlc.MediaPlayer;
import org.videolan.libvlc.util.VLCVideoLayout;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

public class AppInitActivity extends AppCompatActivity {

    private static final String TAG = "AppInit";

    ImageView wifiCheckIcon, liveViewIcon, medBotConIcon, pairedConIcon;
    TextView wifiText, liveViewText, medBotConText, pairedConText, alertMsgText;
    Button btnQuit, btnRetry;

    private String userPwd = "";

    private Handler mHandler = new Handler();

    private VLCVideoLayout mVideoLayout = null;

    private LibVLC mLibVLC = null;
    private MediaPlayer mMediaPlayer = null;

    private String STREAMING_URI = "";
    private String mcuIp = "";

    private String APP_KEY = "";
    private String authKey = "2sMUxEN1xF";

    private SharedPreferences sharedPreferences;

    private static final int READ_PHONE_STATE = 4;

    private int checkCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_app_init);

        wifiCheckIcon = findViewById(R.id.wifiConIcon);
        liveViewIcon = findViewById(R.id.liveVideoIcon);
        medBotConIcon = findViewById(R.id.botConIcon);
        pairedConIcon = findViewById(R.id.pairedIcon);
        wifiText = findViewById(R.id.wifiConText);
        liveViewText = findViewById(R.id.liveVideoText);
        medBotConText = findViewById(R.id.botConText);
        pairedConText = findViewById(R.id.pairedtext);
        mVideoLayout = findViewById(R.id.video_layout);
        btnQuit = findViewById(R.id.btn_quit);
        btnRetry = findViewById(R.id.btn_retry);
        alertMsgText = findViewById(R.id.app_init_alert_msgs);

        sharedPreferences = this.getSharedPreferences(NodeMcuControllerApplication.MYPREFS, Context.MODE_PRIVATE);

        if(sharedPreferences.getString(NodeMcuControllerApplication.spKeyAppKey, "").equals("")) {
            String newAppKey = String.format(Locale.US, "%s_%d", UUID.randomUUID().toString().substring(0, 5), Calendar.getInstance().getTimeInMillis());
            SharedPreferences.Editor spEdit = sharedPreferences.edit();
            spEdit.putString(NodeMcuControllerApplication.spKeyAppKey, newAppKey);
            spEdit.apply();
        }

        APP_KEY = sharedPreferences.getString(NodeMcuControllerApplication.spKeyAppKey, "");

        try {
            userPwd = sharedPreferences.getString(NodeMcuControllerApplication.spKeyUserPwd, PwdHashing.getHashPwd("123"));
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            e.printStackTrace();
        }

        STREAMING_URI = sharedPreferences.getString(NodeMcuControllerApplication.spKeyRtspUri, "");
        mcuIp = sharedPreferences.getString(NodeMcuControllerApplication.spKeyIPAddress, "");

        if(mcuIp.isEmpty()){
            AlertDialog.Builder loginDialog = new AlertDialog.Builder(AppInitActivity.this);

            loginDialog.setTitle(R.string.title_medbot_ip_needed);
            loginDialog.setCancelable(false);
            //loginDialog.setMessage(R.string.alert_msg_permission_override);

            //final EditText mcuIpText = new EditText(AppInitActivity.this);
            View mcuIpLayout = getLayoutInflater().inflate(R.layout.mcu_ip, null);
            loginDialog.setView(mcuIpLayout);
            final EditText mcuIpText = mcuIpLayout.findViewById(R.id.mcu_ip);
            mcuIpText.setText(R.string.default_ip);
            //pwdText.setHint(getString(R.string.text_enter_passcode));

            loginDialog.setPositiveButton(R.string.label_save, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if(mcuIpText.getText().toString().isEmpty()) {
                        Toast.makeText(AppInitActivity.this, R.string.msg_cannot_be_empty, Toast.LENGTH_SHORT).show();
                    } else {

                        SharedPreferences.Editor spEdit = sharedPreferences.edit();
                        spEdit.putString(NodeMcuControllerApplication.spKeyIPAddress, mcuIpText.getText().toString());
                        spEdit.apply();

                        dialog.dismiss();
                        Intent intent = getIntent();
                        finish();
                        startActivity(intent);

                    }
                }
            }).setNegativeButton(R.string.label_exit, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    finishAndRemoveTask();
                }
            });

            if(!this.isFinishing()) {
                loginDialog.show();
            }
        } else {

            // quit button
            btnQuit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(AppInitActivity.this);
                    alertBuilder.setTitle(R.string.alert_title_quit_app)
                            .setMessage(R.string.alert_msg_quit_app)
                            .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    finishAndRemoveTask();
                                }
                            }).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

                    if(!AppInitActivity.this.isFinishing()) {
                        alertBuilder.show();
                    }
                }
            });

            // retry button
            btnRetry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = getIntent();
                    finish();
                    startActivity(intent);
                }
            });

            if(!mcuIp.startsWith("http://")) {
                mcuIp = "http://" + mcuIp;
            }

            Log.d(TAG, "mcu IP: " + mcuIp);

            final ArrayList<String> args = new ArrayList<>();
            args.add("--file-caching=2000");
            args.add("-vvv");
            mLibVLC = new LibVLC(this, args);
            mMediaPlayer = new MediaPlayer(mLibVLC);

            mMediaPlayer.setVolume(0);


            mHandler.postDelayed(wifiCheckRunnable, 1000);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(mMediaPlayer != null) {
            mMediaPlayer.release();
        }
        if(mLibVLC != null) {
            mLibVLC.release();
        }

    }

    private Runnable wifiCheckRunnable = new Runnable() {
        @Override
        public void run() {
            if(checkWifiEnabled()) {
                wifiCheckIcon.setColorFilter(ContextCompat.getColor(AppInitActivity.this, R.color.colorBlueSuccess));
                wifiText.setTextColor(ContextCompat.getColor(AppInitActivity.this, R.color.colorBlueSuccess));

                checkCount++;
                mHandler.postDelayed(paringCheckRunnable, 1000);
            } else {
                wifiCheckIcon.setColorFilter(ContextCompat.getColor(AppInitActivity.this, R.color.colorRedFailed));
                wifiText.setTextColor(ContextCompat.getColor(AppInitActivity.this, R.color.colorRedFailed));

                alertMsgText.setText(R.string.alert_msg_wifi_failed);

                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(AppInitActivity.this);
                alertBuilder.setTitle(R.string.alert_title_wifi_failed)
                        .setMessage(R.string.alert_msg_wifi_failed)
                        .setPositiveButton(R.string.label_open_wifi_settings, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                            }
                        }).setNegativeButton(R.string.label_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                if(!AppInitActivity.this.isFinishing()) {
                    alertBuilder.show();
                }
            }
        }
    };

    private boolean checkWifiEnabled() {
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        if (wifiManager.isWifiEnabled()) {
            // wifi is enabled
            return true;
        }

        return false;
    }

    private Runnable liveViewCheckRunnable = new Runnable() {
        @Override
        public void run() {
            if(videoCheck()){
                liveViewIcon.setColorFilter(ContextCompat.getColor(AppInitActivity.this, R.color.colorBlueSuccess));
                liveViewText.setTextColor(ContextCompat.getColor(AppInitActivity.this, R.color.colorBlueSuccess));

                if(mMediaPlayer.isPlaying()) {
                    mMediaPlayer.stop();
                    //mMediaPlayer.release();
                }
            } else {
                liveViewIcon.setColorFilter(ContextCompat.getColor(AppInitActivity.this, R.color.colorRedFailed));
                liveViewText.setTextColor(ContextCompat.getColor(AppInitActivity.this, R.color.colorRedFailed));
            }

            if(checkCount>=2) {
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent mainActivity = new Intent(AppInitActivity.this, MainActivity.class);
                        finish();
                        startActivity(mainActivity);
                    }
                }, 1000);
            }
        }
    };

    private boolean videoCheck() {
        if(STREAMING_URI.isEmpty()) {
            return false;
        } else {
            try {
                final Media mMedia = new Media(mLibVLC, Uri.parse(STREAMING_URI));
                mMediaPlayer.setMedia(mMedia);
                mMedia.release();
            } catch(Exception e) {
                Log.d(TAG, "VLC video didn't play");
                return false;
            }
        }
        return true;
    }

    private Runnable paringCheckRunnable = new Runnable() {
        @Override
        public void run() {
            if(pairingCheck()) {
                pairedConIcon.setColorFilter(ContextCompat.getColor(AppInitActivity.this, R.color.colorBlueSuccess));
                pairedConText.setTextColor(ContextCompat.getColor(AppInitActivity.this, R.color.colorBlueSuccess));
            } else {
                pairedConIcon.setColorFilter(ContextCompat.getColor(AppInitActivity.this, R.color.colorRedFailed));
                pairedConText.setTextColor(ContextCompat.getColor(AppInitActivity.this, R.color.colorRedFailed));
            }
        }
    };

    private boolean pairingCheck() {
        if(APP_KEY.equals("")) {
            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(AppInitActivity.this);
            alertBuilder.setCancelable(false);
            alertBuilder.setTitle(R.string.alert_title_pairing_failed);
            alertBuilder.setMessage(R.string.alert_msg_pairing_key_empty);
            alertBuilder.setPositiveButton(R.string.label_restart, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    Intent intent = getIntent();
                    finish();
                    startActivity(intent);
                }
            }).setNegativeButton(R.string.label_exit, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    finishAndRemoveTask();
                }
            });

            if(!this.isFinishing()) {
                alertBuilder.show();
            }
        } else {
            RequestQueue queue = Volley.newRequestQueue(AppInitActivity.this);
            String url = String.format(Locale.US, "%s/pair_controller?app_key=%s", mcuIp, APP_KEY);
            Log.d(TAG, "Paring url: " + url);
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            if(response.equals("pairing success")) {
                                pairingCheckCallback(true);
                            } else if(response.equals("already paired")) {
                                AlertDialog.Builder loginDialog = new AlertDialog.Builder(AppInitActivity.this);

                                loginDialog.setTitle(R.string.alert_title_permission_override);

                                View loginLayout = getLayoutInflater().inflate(R.layout.login, null);
                                loginDialog.setView(loginLayout);
                                final EditText pwdText = loginLayout.findViewById(R.id.pwd_text);
                                pwdText.setHint(getString(R.string.text_enter_passcode));

                                loginDialog.setPositiveButton(R.string.label_proceed, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        if(pwdText.getText().toString().isEmpty()) {
                                            Toast.makeText(AppInitActivity.this, R.string.msg_process_failed, Toast.LENGTH_LONG).show();
                                        } else {
                                            String savedPwd = userPwd;
                                            try {
                                                String pwdInputHashed = PwdHashing.getHashPwd((pwdText.getText().toString()));
                                                Log.d(TAG, "passwords: " + pwdText.getText().toString() + " " + pwdInputHashed + " " + savedPwd);
                                                if(pwdInputHashed.equals(savedPwd)) {
                                                    RequestQueue queue = Volley.newRequestQueue(AppInitActivity.this);
                                                    String url = String.format(Locale.US, "%s/pair_controller?admin_auth=%s&app_key=%s", mcuIp, "yes", APP_KEY);
                                                    Log.d(TAG, "paring url2: " + url);
                                                    StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                                                            new Response.Listener<String>() {
                                                                @Override
                                                                public void onResponse(String response) {
                                                                    if(response.equals("pairing success")) {
                                                                        botCheckCallback(true);
                                                                    } else {
                                                                        botCheckCallback(false);
                                                                    }
                                                                    Log.d("VolleyRes", "volley res: " + response);
                                                                }
                                                            }, new Response.ErrorListener() {
                                                        @Override
                                                        public void onErrorResponse(VolleyError error) {
                                                            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(AppInitActivity.this);
                                                            alertBuilder.setCancelable(false);

                                                            View mcuIpLayout = getLayoutInflater().inflate(R.layout.mcu_ip, null);
                                                            alertBuilder.setView(mcuIpLayout);
                                                            final EditText mcuIpText = mcuIpLayout.findViewById(R.id.mcu_ip);
                                                            mcuIpText.setText(R.string.default_ip);

                                                            //alertBuilder.setView(mcuIpText);
                                                            if(!sharedPreferences.getString(NodeMcuControllerApplication.spKeyIPAddress, "").isEmpty()) {
                                                                mcuIpText.setText(sharedPreferences.getString(NodeMcuControllerApplication.spKeyIPAddress, ""));
                                                            }

                                                            alertBuilder.setTitle(R.string.alert_title_pairing_failed)
                                                                    .setMessage(R.string.msg_pwm_sending_failed)
                                                                    .setPositiveButton(R.string.label_retry, new DialogInterface.OnClickListener() {
                                                                        @Override
                                                                        public void onClick(DialogInterface dialog, int which) {
                                                                            if(!mcuIpText.getText().toString().isEmpty()) {
                                                                                SharedPreferences.Editor spEdit = sharedPreferences.edit();
                                                                                spEdit.putString(NodeMcuControllerApplication.spKeyIPAddress, mcuIpText.getText().toString());
                                                                                if(spEdit.commit()) {
                                                                                    dialog.dismiss();
                                                                                    Intent intent = getIntent();
                                                                                    finish();
                                                                                    startActivity(intent);
                                                                                }
                                                                            }
                                                                        }
                                                                    }).setNegativeButton(R.string.label_exit, new DialogInterface.OnClickListener() {
                                                                @Override
                                                                public void onClick(DialogInterface dialog, int which) {
                                                                    dialog.dismiss();
                                                                    finishAndRemoveTask();
                                                                }
                                                            });

                                                            if(!AppInitActivity.this.isFinishing()) {
                                                                alertBuilder.show();
                                                            }
                                                            botCheckCallback(false);
                                                            Log.d("VolleyRes", "volley error: " + error.getMessage());
                                                        }
                                                    }){
                                                        /** Passing some request headers* */
                                                        @Override
                                                        public Map<String, String> getHeaders() {
                                                            HashMap<String, String> headers = new HashMap<>();
                                                            headers.put("MDAuthCheck", authKey);
                                                            return headers;
                                                        }
                                                    };

                                                    queue.add(stringRequest);
                                                } else {
                                                    Toast.makeText(AppInitActivity.this, R.string.msg_process_failed, Toast.LENGTH_SHORT).show();
                                                }

                                                dialog.dismiss();

                                            } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                    }
                                }).setNegativeButton(R.string.label_cancel, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });

                                if(!AppInitActivity.this.isFinishing()) {
                                    loginDialog.show();
                                }

                                Log.d(TAG, "Permission override needed");
                            } else {
                                pairingCheckCallback(false);
                            }
                            Log.d("VolleyRes", "volley res: " + response);
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(AppInitActivity.this);
                    alertBuilder.setCancelable(false);

                    View mcuIpLayout = getLayoutInflater().inflate(R.layout.mcu_ip, null);
                    alertBuilder.setView(mcuIpLayout);
                    final EditText mcuIpText = mcuIpLayout.findViewById(R.id.mcu_ip);
                    mcuIpText.setText(R.string.default_ip);

                    //alertBuilder.setView(mcuIpText);
                    if(!sharedPreferences.getString(NodeMcuControllerApplication.spKeyIPAddress, "").isEmpty()) {
                        mcuIpText.setText(sharedPreferences.getString(NodeMcuControllerApplication.spKeyIPAddress, ""));
                    }

                    alertBuilder.setTitle(R.string.alert_title_medbot_con_failed)
                            .setMessage(R.string.msg_pwm_sending_failed)
                            .setPositiveButton(R.string.label_retry, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if(!mcuIpText.getText().toString().isEmpty()) {
                                        SharedPreferences.Editor spEdit = sharedPreferences.edit();
                                        spEdit.putString(NodeMcuControllerApplication.spKeyIPAddress, mcuIpText.getText().toString());
                                        if(spEdit.commit()) {
                                            dialog.dismiss();
                                            Intent intent = getIntent();
                                            finish();
                                            startActivity(intent);
                                        }
                                    }
                                }
                            }).setNegativeButton(R.string.label_exit, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            finishAndRemoveTask();
                        }
                    });

                    if(!AppInitActivity.this.isFinishing()) {
                        alertBuilder.show();
                    }

                    pairingCheckCallback(false);

                    Log.d("VolleyRes", "volley error: " + error.getMessage() );
                }
            }){
                /** Passing some request headers* */
                @Override
                public Map<String, String> getHeaders() {
                    HashMap<String, String> headers = new HashMap<>();
                    headers.put("MDAuthCheck", authKey);
                    return headers;
                }
            };

            queue.add(stringRequest);
        }

        return true;
    }

    private Runnable botCheckRunnable = new Runnable() {
        @Override
        public void run() {
            botCheck();
        }
    };

    private void botCheck() {
        RequestQueue queue = Volley.newRequestQueue(AppInitActivity.this);
        String url = String.format("%s/con_check", mcuIp);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response.equals("con success")) {
                            botCheckCallback(true);
                        } else {
                            alertOnBotCheck();
                            botCheckCallback(false);
                        }
                    Log.d("VolleyRes", "volley res: " + response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("VolleyRes", "volley error: " + error.getMessage());
                alertOnBotCheck();
                botCheckCallback(false);
            }
        }){
            /** Passing some request headers* */
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("MDAuthCheck", authKey);
                return headers;
            }
        };

        queue.add(stringRequest);

    }

    private void alertOnBotCheck() {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(AppInitActivity.this);
        alertBuilder.setCancelable(false);

        View mcuIpLayout = getLayoutInflater().inflate(R.layout.mcu_ip, null);
        alertBuilder.setView(mcuIpLayout);
        final EditText mcuIpText = mcuIpLayout.findViewById(R.id.mcu_ip);
        mcuIpText.setText(R.string.default_ip);

        if(!sharedPreferences.getString(NodeMcuControllerApplication.spKeyIPAddress, "").isEmpty()) {
            mcuIpText.setText(sharedPreferences.getString(NodeMcuControllerApplication.spKeyIPAddress, ""));
        }

        //alertBuilder.setView(mcuIpText);

        alertBuilder.setTitle(R.string.alert_title_medbot_con_failed)
                .setMessage(R.string.msg_check_medbot_con)
                .setPositiveButton(R.string.label_retry, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(!mcuIpText.getText().toString().isEmpty()) {
                            SharedPreferences.Editor spEdit = sharedPreferences.edit();
                            spEdit.putString(NodeMcuControllerApplication.spKeyIPAddress, mcuIpText.getText().toString());
                            if(spEdit.commit()) {
                                dialog.dismiss();
                                Intent intent = getIntent();
                                finish();
                                startActivity(intent);
                            }
                        }
                    }
                }).setNegativeButton(R.string.label_exit, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finishAndRemoveTask();
            }
        });

        if(!AppInitActivity.this.isFinishing()) {
            alertBuilder.show();
        }
    }

    protected void botCheckCallback(boolean botCheckStatus) {
        if(botCheckStatus) {
            checkCount++;
            medBotConIcon.setColorFilter(ContextCompat.getColor(this, R.color.colorBlueSuccess));
            medBotConText.setTextColor(ContextCompat.getColor(this, R.color.colorBlueSuccess));

            mHandler.postDelayed(liveViewCheckRunnable, 1000);
        } else {
            medBotConIcon.setColorFilter(ContextCompat.getColor(this, R.color.colorRedFailed));
            medBotConText.setTextColor(ContextCompat.getColor(this, R.color.colorRedFailed));
        }
    }

    protected void pairingCheckCallback(boolean paringCheckStatus) {
        if(paringCheckStatus) {
            pairedConIcon.setColorFilter(ContextCompat.getColor(this, R.color.colorBlueSuccess));
            pairedConText.setTextColor(ContextCompat.getColor(this, R.color.colorBlueSuccess));

            mHandler.postDelayed(botCheckRunnable, 1000);

        } else {
            pairedConIcon.setColorFilter(ContextCompat.getColor(this, R.color.colorRedFailed));
            pairedConText.setTextColor(ContextCompat.getColor(this, R.color.colorRedFailed));
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {

            case READ_PHONE_STATE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    finishAndRemoveTask();
                }
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

}
