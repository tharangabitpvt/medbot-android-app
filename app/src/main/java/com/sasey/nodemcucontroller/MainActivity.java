package com.sasey.nodemcucontroller;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Bundle;
import android.text.Layout;
import android.text.format.Formatter;
import android.text.method.PasswordTransformationMethod;
import android.util.ArrayMap;
import android.util.Log;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.videolan.libvlc.LibVLC;
import org.videolan.libvlc.Media;
import org.videolan.libvlc.MediaPlayer;
import org.videolan.libvlc.util.VLCVideoLayout;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.InetAddress;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static android.content.Intent.ACTION_BATTERY_CHANGED;
import static android.net.wifi.WifiManager.NETWORK_STATE_CHANGED_ACTION;
import static android.net.wifi.WifiManager.WIFI_STATE_DISABLING;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();

    ImageButton btnForward;
    ImageButton btnBackward;
    ImageButton btnTurnLeft;
    ImageButton btnTurnRight;
    ImageButton btnSettings;
    ImageButton playButton;
    ImageButton volumeButton;
    ImageButton guideLinesOnOffButton;
    ImageButton stopButton, stopButtonLeft;

    ImageView videoOffOverlay;
    ImageView fingerTapRing_F;
    ImageView fingerTapRing_B;
    ImageView fingerTapRing_R;
    ImageView fingerTapRing_L;
    ImageView guideLineLeft, guideLineRight, wifiSignalLevel, batteryLevel;
    LinearLayout msgOverlay;

    //TextView speedValue;

    String mcuIp = "";

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor spEditor;

    private String currentSpeedValue = "slow";

    private static final String DEBUG_TAG = "Gestures";

    private static final boolean USE_TEXTURE_VIEW = false;
    private static final boolean ENABLE_SUBTITLES = true;
    private static String STREAMING_URI = "";

    private VLCVideoLayout mVideoLayout = null;

    private LibVLC mLibVLC = null;
    private MediaPlayer mMediaPlayer = null;

    private static boolean isVideoEnabled = true;

    private static final int AUDIO_LEVEL = 70;
    private int VIDEO_STREAM_CACHING = 100;

    RequestQueue queue;

    private String APP_KEY = "";
    private String authKey = "2sMUxEN1xF";

    private boolean showGuideLines = false;

    private static final String V_TAG_AUTH = "";
    private static final String V_TAG_STOP = "STOP_REQ";
    private static final String V_TAG_FORWARD = "GO_FORWARD_REQ";
    private static final String V_TAG_BACKWARD = "GO_BACK_REQ";
    private static final String V_TAG_TURN_LEFT = "TURN_LEFT_REQ";
    private static final String V_TAG_TURN_RIGHT = "TURN_RIGHT_REQ";
    private static final String V_TAG_SAVE_SETTINGS = "SAVE_SETTINGS";

    private MedbotReceiver mbReceiver;
    private boolean isRegisterReceiver = false;
    public WifiManager wifiManager;

    ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(3);

    private boolean signalIsGood = true;

    private String forwardOrBackwardPressed = "s";
    private long timeRequestSent = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "VideoVLC -- onCreate -- START ------------");
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        sharedPreferences = this.getSharedPreferences(NodeMcuControllerApplication.MYPREFS, Context.MODE_PRIVATE);

        APP_KEY = sharedPreferences.getString(NodeMcuControllerApplication.spKeyAppKey, "");

        if(APP_KEY.equals("")) {
            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(MainActivity.this);
            alertBuilder.setTitle(R.string.msg_unexpected_error)
                    .setMessage(R.string.msg_close_and_open_app)
                    .setPositiveButton(R.string.label_close, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            finishAndRemoveTask();
                        }
                    });

            if(!MainActivity.this.isFinishing()) {
                alertBuilder.show();
            }
        }

        initSharedPrefs();
        initViews();

        final Runnable checkWifiStrength = new Runnable() {
            @Override
            public void run() {
                int numberOfLevels = 5;
                WifiInfo wifiInfo = wifiManager.getConnectionInfo();
                int level = WifiManager.calculateSignalLevel(wifiInfo.getRssi(), numberOfLevels);
                //Log.d(TAG, "WIFI signal level " + level + " / " + numberOfLevels);

                Drawable wifiIcon;
                switch(level) {
                    case 5:
                        wifiIcon = ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_signal_wifi_4_bar_black_24dp);
                        break;
                    case 4:
                        wifiIcon = ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_signal_wifi_3_bar_black_24dp);
                        break;
                    case 3:
                        wifiIcon = ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_signal_wifi_2_bar_black_24dp);
                        break;
                    case 2:
                        wifiIcon = ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_signal_wifi_1_bar_black_24dp);
                        break;
                    default:
                        wifiIcon = ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_signal_wifi_0_bar_black_24dp);
                }

                if(level < 2) {
                    msgOverlay.setVisibility(View.VISIBLE);
                    signalIsGood = false;
                    sendStopRequest();
                } else {
                    signalIsGood = true;
                    msgOverlay.setVisibility(View.GONE);
                }

                wifiSignalLevel.setImageDrawable(wifiIcon);
            }
        };

        scheduler.scheduleAtFixedRate(checkWifiStrength, 0, 500, TimeUnit.MILLISECONDS);

        mMediaPlayer.attachViews(mVideoLayout, null, ENABLE_SUBTITLES, USE_TEXTURE_VIEW);
        mMediaPlayer.setVolume(0);
    }

    @Override
    protected void onResume() {
        super.onResume();

        sharedPreferences = this.getSharedPreferences(NodeMcuControllerApplication.MYPREFS, Context.MODE_PRIVATE);
    }

    @Override
    protected void onStart() {
        super.onStart();

        /*try {
            final Media media = new Media(mLibVLC, getAssets().openFd(ASSET_FILENAME));
            mMediaPlayer.setMedia(media);
            media.release();
        } catch (IOException e) {
            throw new RuntimeException("Invalid asset folder");
        }
        mMediaPlayer.play();*/

    }

    @Override
    protected void onStop() {
        super.onStop();

        mMediaPlayer.stop();
        mMediaPlayer.detachViews();
    }

    @Override
    protected void onPause() {
        super.onPause();

        sendStopRequest();

        fingerTapRing_F.setVisibility(View.GONE);
        fingerTapRing_B.setVisibility(View.GONE);
        fingerTapRing_L.setVisibility(View.GONE);
        fingerTapRing_R.setVisibility(View.GONE);
    }

    public RequestQueue getRequestQueue() {
        if (queue == null)
            queue = Volley.newRequestQueue(getApplicationContext());
        return queue;
    }

    public void addToRequestQueue(Request request, String tag) {
        long timeNow = Calendar.getInstance().getTimeInMillis();
        if(forwardOrBackwardPressed.equals("s") || timeNow - timeRequestSent > 300) {
            if (getRequestQueue() != null) {
                getRequestQueue().cancelAll(tag);
           /* getRequestQueue().cancelAll(new RequestQueue.RequestFilter() {
                @Override
                public boolean apply(Request<?> request) {
                    return true;
                }
            });*/
            }
            request.setTag(tag);
            request.setShouldCache(false);
            request.setRetryPolicy(new DefaultRetryPolicy(
                    0,
                    0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            if (signalIsGood) {
                getRequestQueue().add(request);
                timeRequestSent = Calendar.getInstance().getTimeInMillis();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        sendStopRequest();

        // MediaCodec opaque direct rendering should not be used anymore since there is no surface to attach.
        mMediaPlayer.release();
        mLibVLC.release();

        fingerTapRing_F.setVisibility(View.GONE);
        fingerTapRing_B.setVisibility(View.GONE);
        fingerTapRing_L.setVisibility(View.GONE);
        fingerTapRing_R.setVisibility(View.GONE);

        scheduler.shutdown();

        unregisterReceiver(mbReceiver);

    }

    private void sendStopRequest () {
        String url = String.format(Locale.US, "%s/stop?app_key=%s", mcuIp, APP_KEY);

        Log.d(DEBUG_TAG, "on Pause called mcuURL: " + url);
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("VolleyRes", "volley res: " + response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("VolleyRes", "volley error: " + error.getMessage());
            }
        }){
            /** Passing some request headers* */
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("MDAuthCheck", authKey);
                return headers;
            }
        };

        // Add the request to the RequestQueue.
        addToRequestQueue(stringRequest, V_TAG_STOP);
    }


    @SuppressLint("ClickableViewAccessibility")
    private void initViews() {
        btnForward = findViewById(R.id.btnForward);
        btnBackward = findViewById(R.id.btnBackword);
        btnTurnLeft = findViewById(R.id.btnTurnLeft);
        btnTurnRight = findViewById(R.id.btnTurnRight);
        btnSettings = findViewById(R.id.btn_settings);
        videoOffOverlay = findViewById(R.id.video_off_overlay);
        fingerTapRing_F = findViewById(R.id.finger_tap_ring_f);
        fingerTapRing_F.setVisibility(View.GONE);
        fingerTapRing_B = findViewById(R.id.finger_tap_ring_b);
        fingerTapRing_B.setVisibility(View.GONE);
        fingerTapRing_R = findViewById(R.id.finger_tap_ring_r);
        fingerTapRing_R.setVisibility(View.GONE);
        fingerTapRing_L = findViewById(R.id.finger_tap_ring_l);
        fingerTapRing_L.setVisibility(View.GONE);
        volumeButton = findViewById(R.id.video_volume_button);
        //speedValue = findViewById(R.id.speed_value);
        guideLinesOnOffButton = findViewById(R.id.guide_lines_on_off_btn);
        guideLineLeft = findViewById(R.id.guide_line_left);
        guideLineRight = findViewById(R.id.guide_line_right);
        wifiSignalLevel = findViewById(R.id.wifi_signal_strength);
        msgOverlay = findViewById(R.id.msg_overlay);
        msgOverlay.bringToFront();
        //msgOverlay.setVisibility(View.GONE);
        stopButton = findViewById(R.id.stop_button);
        stopButtonLeft = findViewById(R.id.stop_button_left);
        batteryLevel = findViewById(R.id.battery_level);

        wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        mbReceiver = new MedbotReceiver();
        registerServiceReceiver();

        onOffGuideLines(showGuideLines);

        videoOffOverlay.setVisibility(View.VISIBLE);

        playButton = findViewById(R.id.play_btn);

        isVideoEnabled = sharedPreferences.getBoolean(NodeMcuControllerApplication.spKeyIsVideoEnabled, true);

        timeRequestSent = Calendar.getInstance().getTimeInMillis();

        //int savedSpeed = sharedPreferences.getInt(NodeMcuControllerApplication.spKeyForwardPWM, 200);
        /*if(savedSpeed <= 150) {
            currentSpeedValue = getString(R.string.label_low);
        } else if(savedSpeed <= 200) {
            currentSpeedValue = getString(R.string.label_medium);
        } else {
            currentSpeedValue = getString(R.string.label_high);
        }*/

        //speedValue.setText(currentSpeedValue);

        playButton.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_videocam_off_black_24dp));
        playButton.setColorFilter(ContextCompat.getColor(this, R.color.colorRed));
        volumeButton.setEnabled(false);

        mcuIp = sharedPreferences.getString(NodeMcuControllerApplication.spKeyIPAddress, "");

        if(!mcuIp.startsWith("http://")) {
            mcuIp = "http://" + mcuIp;
        }

        // send settings values
        final int paramFPWM_L = sharedPreferences.getInt(NodeMcuControllerApplication.spKeyForwardPWM_L, 180);
        final int paramFPWD_R = sharedPreferences.getInt(NodeMcuControllerApplication.spKeyForwardPWM_R, 180);
        final int paramBackPwm_L = sharedPreferences.getInt(NodeMcuControllerApplication.spKeyBackPwmLeft, 170);
        final int paramBackPwm_R = sharedPreferences.getInt(NodeMcuControllerApplication.spKeyBackPwmRight, 170);

        String url =  String.format(Locale.US, "%s/save_settings", MainActivity.this.mcuIp);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        //textView.setText("Response is: "+ response.substring(0,500));
                        Log.d("VolleyRes", "volley res: " + response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //textView.setText("That didn't work!");
                Log.d("VolleyRes", "volley error: " + error.getMessage());
                if((error.networkResponse != null) && (error.networkResponse.statusCode == 401)) {
                    unAuthAction();
                }
            }
        }){
            /** Passing some request headers* */
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("MDAuthCheck", authKey);
                return headers;
            }

            @Override
            public Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<>();
                params.put("f_pwm_l", String.valueOf(paramFPWM_L));
                params.put("f_pwm_r", String.valueOf(paramFPWD_R));
                params.put("b_pwm_l", String.valueOf(paramBackPwm_L));
                params.put("b_pwm_r", String.valueOf(paramBackPwm_R));
                params.put("app_key", APP_KEY);

                return params;
            }
        };

        // Add the request to the RequestQueue.
        addToRequestQueue(stringRequest, V_TAG_SAVE_SETTINGS);

        STREAMING_URI = sharedPreferences.getString(NodeMcuControllerApplication.spKeyRtspUri, "");

        final ArrayList<String> args = new ArrayList<>();
        //args.add("--file-caching=2000");
        args.add(String.format(Locale.US, "--network-caching=%d", VIDEO_STREAM_CACHING));
        //args.add(String.format(Locale.US, "--live-caching=%d", VIDEO_STREAM_CACHING));
        //args.add("--aout=opensles");
        args.add("-vvv");
        mLibVLC = new LibVLC(this, args);
        mMediaPlayer = new MediaPlayer(mLibVLC);

        if(mMediaPlayer.getVolume() > 0) {
            volumeButton.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_volume_up_black_24dp));
        } else {
            volumeButton.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_volume_off_black_24dp));
        }

        if(!STREAMING_URI.isEmpty()) {
            playButton.setEnabled(true);
            volumeButton.setEnabled(true);

            setPlayerMedia();
        } else {
            playButton.setEnabled(false);
            volumeButton.setEnabled(false);
        }

        mVideoLayout = findViewById(R.id.video_layout);

        btnForward.setOnTouchListener(new View.OnTouchListener() {

            //RequestQueue queue = Volley.newRequestQueue(MainActivity.this);
            String url = "";

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                int touchAction = event.getAction();
                //Log.d(DEBUG_TAG, "ON ACTION: " + touchAction);

                // ignore scrolling finger
                if(touchAction == MotionEvent.ACTION_MOVE) {
                    return false;
                }

                String V_TAG = V_TAG_STOP;

                if(touchAction == MotionEvent.ACTION_DOWN) { // if touch down
                    url = String.format(Locale.US, "%s/go_forward?app_key=%s&fpwml=%d&fpwmr=%d&rpwml=%d&rpwmr=%d", mcuIp, APP_KEY, paramFPWM_L, paramFPWD_R, paramBackPwm_L, paramBackPwm_R);
                    fingerTapRing_F.setVisibility(View.VISIBLE);
                    V_TAG = V_TAG_FORWARD;
                    forwardOrBackwardPressed = "f";
                } else if(touchAction == MotionEvent.ACTION_UP || touchAction == MotionEvent.ACTION_CANCEL) {
                    url = String.format(Locale.US, "%s/stop?fpwml=%d&fpwmr=%d&rpwml=%d&rpwmr=%d", mcuIp, paramFPWM_L, paramFPWD_R, paramBackPwm_L, paramBackPwm_R);
                    fingerTapRing_F.setVisibility(View.GONE);
                    forwardOrBackwardPressed = "s";
                }

                Log.d(DEBUG_TAG, "mcuURL: " + url);
                // Request a string response from the provided URL.
                StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.d("VolleyRes", "volley res: " + response);
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if((error.networkResponse != null) && (error.networkResponse.statusCode == 401)) {
                            unAuthAction();
                        }
                        Log.d("VolleyRes", "volley error: " + error.getMessage());
                    }
                }){
                    /** Passing some request headers* */
                    @Override
                    public Map<String, String> getHeaders() {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put("MDAuthCheck", authKey);
                        return headers;
                    }
                };

                // Add the request to the RequestQueue.
                addToRequestQueue(stringRequest, V_TAG);
                return true;
            }
        });

        btnBackward.setOnTouchListener(new View.OnTouchListener() {

            //RequestQueue queue = Volley.newRequestQueue(MainActivity.this);
            String url = "";

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int touchAction = event.getAction();
                //Log.d(DEBUG_TAG, "ON ACTION: " + touchAction);

                // ignore scrolling finger
                if(touchAction == MotionEvent.ACTION_MOVE) {
                    return false;
                }

                String V_TAG = V_TAG_STOP;
                final HashMap<String, String> paramsGoBackward = new HashMap<>();

                if(touchAction == MotionEvent.ACTION_DOWN) { // if touch down
                    url = String.format(Locale.US, "%s/go_backward?app_key=%s&fpwml=%d&fpwmr=%d&rpwml=%d&rpwmr=%d", mcuIp, APP_KEY, paramFPWM_L, paramFPWD_R, paramBackPwm_L, paramBackPwm_R);
                    fingerTapRing_B.setVisibility(View.VISIBLE);
                    forwardOrBackwardPressed = "b";
                    V_TAG = V_TAG_BACKWARD;
                } else if(touchAction == MotionEvent.ACTION_UP || touchAction == MotionEvent.ACTION_CANCEL) {
                    url = String.format(Locale.US, "%s/stop?fpwml=%d&fpwmr=%d&rpwml=%d&rpwmr=%d", mcuIp, paramFPWM_L, paramFPWD_R, paramBackPwm_L, paramBackPwm_R);
                    fingerTapRing_B.setVisibility(View.GONE);
                    forwardOrBackwardPressed = "s";
                }

                Log.d(DEBUG_TAG, "mcuURL: " + url);
                // Request a string response from the provided URL.
                StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.d("VolleyRes", "volley res: " + response);
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if((error.networkResponse != null) && (error.networkResponse.statusCode == 401)) {
                            unAuthAction();
                        }
                        Log.d("VolleyRes", "volley error: " + error.getMessage());
                    }
                }){
                    /** Passing some request headers* */
                    @Override
                    public Map<String, String> getHeaders() {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put("MDAuthCheck", authKey);
                        return headers;
                    }
                };

                // Add the request to the RequestQueue.

                addToRequestQueue(stringRequest, V_TAG);
                return true;
            }
        });

        btnTurnLeft.setOnTouchListener(new View.OnTouchListener() {

            //RequestQueue queue = Volley.newRequestQueue(MainActivity.this);
            String url = "";

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int touchAction = event.getAction();
                //Log.d(DEBUG_TAG, "ON ACTION: " + touchAction);

                // ignore scrolling finger
                if(touchAction == MotionEvent.ACTION_MOVE) {
                    return false;
                }

                String V_TAG = V_TAG_STOP;
                final HashMap<String, String> paramsTurnLeft = new HashMap<>();

                if(touchAction == MotionEvent.ACTION_DOWN) { // if touch down
                    if(!forwardOrBackwardPressed.equals("s")) {
                        url = String.format(Locale.US, "%s/turn_left?smooth_turn=%s&direction=%s&app_key=%s&fpwml=%d&fpwmr=%d&rpwml=%d&rpwmr=%d", mcuIp, "true", forwardOrBackwardPressed, APP_KEY, paramFPWM_L, paramFPWD_R, paramBackPwm_L, paramBackPwm_R);

                    } else {
                        url = String.format(Locale.US, "%s/turn_left?smooth_turn=false&app_key=%s&fpwml=%d&fpwmr=%d&rpwml=%d&rpwmr=%d", mcuIp, APP_KEY, paramFPWM_L, paramFPWD_R, paramBackPwm_L, paramBackPwm_R);
                    }

                    V_TAG = V_TAG_TURN_LEFT;
                    fingerTapRing_L.setVisibility(View.VISIBLE);

                } else if(touchAction == MotionEvent.ACTION_UP || touchAction == MotionEvent.ACTION_CANCEL) {
                    if(forwardOrBackwardPressed.equals("f")) {
                        url = String.format(Locale.US, "%s/go_forward?app_key=%s&fpwml=%d&fpwmr=%d&rpwml=%d&rpwmr=%d", mcuIp, APP_KEY, paramFPWM_L, paramFPWD_R, paramBackPwm_L, paramBackPwm_R);
                        V_TAG = V_TAG_FORWARD;
                    } else if (forwardOrBackwardPressed.equals("b")) {
                        url = String.format(Locale.US, "%s/go_backward?app_key=%s&fpwml=%d&fpwmr=%d&rpwml=%d&rpwmr=%d", mcuIp, APP_KEY, paramFPWM_L, paramFPWD_R, paramBackPwm_L, paramBackPwm_R);
                        V_TAG = V_TAG_BACKWARD;
                    } else {
                        url = String.format(Locale.US, "%s/stop?app_key=%s", mcuIp, APP_KEY);
                        forwardOrBackwardPressed = "s";
                        V_TAG = V_TAG_STOP;
                    }

                    fingerTapRing_L.setVisibility(View.GONE);
                }

                Log.d(DEBUG_TAG, "mcuURL: " + url);
                // Request a string response from the provided URL.
                StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.d("VolleyRes", "volley res: " + response);
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if((error.networkResponse != null) && (error.networkResponse.statusCode == 401)) {
                            unAuthAction();
                        }
                        Log.d("VolleyRes", "volley error: " + error.getMessage());
                    }
                }){
                    /** Passing some request headers* */
                    @Override
                    public Map<String, String> getHeaders() {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put("MDAuthCheck", authKey);
                        return headers;
                    }
                };

                // Add the request to the RequestQueue.
                addToRequestQueue(stringRequest, V_TAG);
                return true;
            }
        });

        btnTurnRight.setOnTouchListener(new View.OnTouchListener() {

            //RequestQueue queue = Volley.newRequestQueue(MainActivity.this);
            String url = "";

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int touchAction = event.getAction();
                //Log.d(DEBUG_TAG, "ON ACTION: " + touchAction);

                // ignore scrolling finger
                if(touchAction == MotionEvent.ACTION_MOVE) {
                    return false;
                }

                String V_TAG = V_TAG_STOP;
                if(touchAction == MotionEvent.ACTION_DOWN) { // if touch down
                    if(!forwardOrBackwardPressed.equals("s")) {
                        url = String.format(Locale.US, "%s/turn_right?smooth_turn=%s&direction=%s&app_key=%s&fpwml=%d&fpwmr=%d&rpwml=%d&rpwmr=%d", mcuIp, "true", forwardOrBackwardPressed, APP_KEY, paramFPWM_L, paramFPWD_R, paramBackPwm_L, paramBackPwm_R);
                    } else {
                        url = String.format(Locale.US, "%s/turn_right?smooth_turn=false&app_key=%s&fpwml=%d&fpwmr=%d&rpwml=%d&rpwmr=%d", mcuIp, APP_KEY, paramFPWM_L, paramFPWD_R, paramBackPwm_L, paramBackPwm_R);
                    }
                    fingerTapRing_R.setVisibility(View.VISIBLE);
                    V_TAG = V_TAG_TURN_RIGHT;
                } else if(touchAction == MotionEvent.ACTION_UP || touchAction == MotionEvent.ACTION_CANCEL) {
                    if(forwardOrBackwardPressed.equals("f")) {
                        url = String.format(Locale.US, "%s/go_forward?app_key=%s&fpwml=%d&fpwmr=%d&rpwml=%d&rpwmr=%d", mcuIp, APP_KEY, paramFPWM_L, paramFPWD_R, paramBackPwm_L, paramBackPwm_R);
                        V_TAG = V_TAG_FORWARD;
                    } else if (forwardOrBackwardPressed.equals("b")) {
                        url = String.format(Locale.US, "%s/go_backward?app_key=%s&fpwml=%d&fpwmr=%d&rpwml=%d&rpwmr=%d", mcuIp, APP_KEY, paramFPWM_L, paramFPWD_R, paramBackPwm_L, paramBackPwm_R);
                        V_TAG = V_TAG_BACKWARD;
                    } else {
                        url = String.format(Locale.US, "%s/stop?app_key=%s", mcuIp, APP_KEY);;
                        forwardOrBackwardPressed = "s";
                        V_TAG = V_TAG_STOP;
                    }
                    fingerTapRing_R.setVisibility(View.GONE);
                }

                Log.d(DEBUG_TAG, "mcuURL: " + url);
                // Request a string response from the provided URL.
                StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.d("VolleyRes", "volley res: " + response);
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if((error.networkResponse != null) && (error.networkResponse.statusCode == 401)) {
                            unAuthAction();
                        }
                        Log.d("VolleyRes", "volley error: " + error.getMessage());
                    }
                }){
                    /** Passing some request headers* */
                    @Override
                    public Map<String, String> getHeaders() {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put("MDAuthCheck", authKey);
                        return headers;
                    }
                };

                // Add the request to the RequestQueue.
                addToRequestQueue(stringRequest, V_TAG);
                return true;
            }
        });

        btnSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder loginDialog = new AlertDialog.Builder(MainActivity.this);

                loginDialog.setTitle(R.string.title_login);

                View loginLayout = getLayoutInflater().inflate(R.layout.login, null);
                loginDialog.setView(loginLayout);
                final EditText pwdText = loginLayout.findViewById(R.id.pwd_text);

                loginDialog.setPositiveButton(R.string.label_login, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(pwdText.getText().toString().isEmpty()) {
                            Toast.makeText(MainActivity.this, R.string.msg_login_failed, Toast.LENGTH_LONG).show();
                        } else {
                            String savedPwd = sharedPreferences.getString(NodeMcuControllerApplication.spKeyAdminPwd, "");
                            try {
                                String pwdInputHashed = PwdHashing.getHashPwd((pwdText.getText().toString()));
                                Log.d(DEBUG_TAG, "passwords: " + pwdText.getText().toString() + " " + pwdInputHashed + " " + savedPwd);
                                if(pwdInputHashed.equals(savedPwd)) {
                                    Intent settingsView = new Intent(MainActivity.this, Settings.class);
                                    startActivity(settingsView);
                                } else {
                                    Toast.makeText(MainActivity.this, R.string.msg_login_failed, Toast.LENGTH_LONG).show();
                                }

                                dialog.dismiss();

                            } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }).setNegativeButton(R.string.label_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();


            }
        });

        guideLinesOnOffButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOffGuideLines(!showGuideLines);
            }
        });

        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendStopRequest();
            }
        });
        stopButtonLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendStopRequest();
            }
        });

        PlayVideoAsyncTask playVideoAsyncTask = new PlayVideoAsyncTask(MainActivity.this);
        playVideoAsyncTask.execute();
    }

    private void onOffGuideLines(boolean show) {

        if(show) {
            guideLineLeft.setVisibility(View.VISIBLE);
            guideLineRight.setVisibility(View.VISIBLE);
            guideLinesOnOffButton.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_location_searching_black_24dp));
            showGuideLines = true;
        } else {
            guideLineLeft.setVisibility(View.GONE);
            guideLineRight.setVisibility(View.GONE);
            guideLinesOnOffButton.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_location_disabled_black_24dp));
            showGuideLines = false;
        }

    }

    private void setPlayerMedia() {
        try {
            final Media media = new Media(mLibVLC, Uri.parse(STREAMING_URI));
            media.setHWDecoderEnabled(true, false);
            media.addOption(":network-caching=" + VIDEO_STREAM_CACHING); // 200
            media.addOption(":clock-jitter=0");
            media.addOption(":clock-synchro=0");

            mMediaPlayer.setMedia(media);
            media.release();
        } catch (Exception e) {
            Log.d(TAG, "VLC failed: " + e.getMessage());
            Toast.makeText(MainActivity.this, R.string.msg_video_failed, Toast.LENGTH_SHORT).show();
        }
    }

    protected void videoOnOff() {
        spEditor = sharedPreferences.edit();
        Log.d(TAG, "Video ON/OFF triggered");
        if(mMediaPlayer.isPlaying()){

            mMediaPlayer.stop();
            mMediaPlayer.setVolume(0);
            spEditor.putBoolean(NodeMcuControllerApplication.spKeyIsVideoEnabled, true);
            playButton.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_videocam_off_black_24dp));
            playButton.setColorFilter(ContextCompat.getColor(this, R.color.colorRed));
            //playButton.setEnabled(false);
            volumeButton.setEnabled(false);
            videoOffOverlay.setVisibility(View.VISIBLE);
            onOffGuideLines(false);

        } else {
            try {
                //setPlayerMedia();
                mMediaPlayer.play();
                mMediaPlayer.setVolume(0);
                spEditor.putBoolean(NodeMcuControllerApplication.spKeyIsVideoEnabled, false);
                playButton.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_videocam_black_24dp));
                playButton.setColorFilter(ContextCompat.getColor(this, R.color.colorWhite));
                playButton.setEnabled(true);
                volumeButton.setEnabled(true);
                videoOffOverlay.setVisibility(View.GONE);
                onOffGuideLines(true);
            } catch (Exception e) {
                Toast.makeText(MainActivity.this, R.string.msg_video_failed, Toast.LENGTH_SHORT).show();
            }
        }

        spEditor.apply();
    }

    private void initSharedPrefs() {
        spEditor = sharedPreferences.edit();

        if(!sharedPreferences.contains(NodeMcuControllerApplication.spKeyAdminUsername)) {
            spEditor.putString(NodeMcuControllerApplication.spKeyAdminUsername, "admin");
        }

        if(!sharedPreferences.contains(NodeMcuControllerApplication.spKeyAdminPwd)) {
            try {
                String hashedPwd = PwdHashing.getHashPwd("123456");
                spEditor.putString(NodeMcuControllerApplication.spKeyAdminPwd, hashedPwd);
            } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
                e.printStackTrace();
            }
        }

        if(!sharedPreferences.contains(NodeMcuControllerApplication.spKeyForwardPWM_L)) {
            spEditor.putInt(NodeMcuControllerApplication.spKeyForwardPWM_L, 180);
        }

        if(!sharedPreferences.contains(NodeMcuControllerApplication.spKeyForwardPWM_R)) {
            spEditor.putInt(NodeMcuControllerApplication.spKeyForwardPWM_R, 180);
        }

        if(!sharedPreferences.contains(NodeMcuControllerApplication.spKeyIPAddress)) {
            spEditor.putString(NodeMcuControllerApplication.spKeyIPAddress, getString(R.string.default_ip));
        }

        if(!sharedPreferences.contains(NodeMcuControllerApplication.spKeyRtspUri)) {
            spEditor.putString(NodeMcuControllerApplication.spKeyRtspUri, "rtsp://admin:SHBOEW@192.168.8.110:554/H.264");
        }

        if(!sharedPreferences.contains(NodeMcuControllerApplication.spKeyIsVideoEnabled)) {
            spEditor.putBoolean(NodeMcuControllerApplication.spKeyIsVideoEnabled, true);
        }

        if(!sharedPreferences.contains(NodeMcuControllerApplication.spKeyBackPwmLeft)) {
            spEditor.putInt(NodeMcuControllerApplication.spKeyBackPwmLeft, 170);
        }

        if(!sharedPreferences.contains(NodeMcuControllerApplication.spKeyBackPwmRight)) {
            spEditor.putInt(NodeMcuControllerApplication.spKeyBackPwmRight, 170);
        }

        spEditor.apply();
    }


    private void registerServiceReceiver() {
        IntentFilter filter = new IntentFilter();

        //filter.addAction("onDescriptorWrite");
        //filter.addAction(String.valueOf(WIFI_STATE_DISABLING));
        //filter.addAction(Intent.ACTION_POWER_DISCONNECTED);
        //filter.addAction("com.jstylelife.ble.service.onDescriptorWrite");
        filter.addAction(ACTION_BATTERY_CHANGED);
        registerReceiver(mbReceiver, filter);

        isRegisterReceiver = true;
        Log.i(TAG, "REGISTER RECEIVER");
    }
    class MedbotReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            //Log.d(TAG, "BC receiver running. Action: " + action);

            switch(action) {
                case NETWORK_STATE_CHANGED_ACTION:
                    if(wifiManager.getWifiState() == WIFI_STATE_DISABLING) {
                        sendStopRequest();
                    }
                    break;

                case ACTION_BATTERY_CHANGED:
                    int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
                    //Log.d(TAG, "Battery level: " + level);
                    if(level >= 90) {
                        batteryLevel.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_battery_full_black_24dp));
                    } else if(level >= 80) {
                        batteryLevel.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_battery_80_black_24dp));
                    }else if(level >= 60) {
                        batteryLevel.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_battery_60_black_24dp));
                    } else if(level >= 50) {
                        batteryLevel.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_battery_50_black_24dp));
                    } else if(level >= 30) {
                        batteryLevel.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_battery_30_black_24dp));
                    } else if(level >= 20) {
                        batteryLevel.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_battery_20_black_24dp));
                    } else {
                        batteryLevel.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_battery_alert_black_24dp));
                        batteryLevel.setColorFilter(R.color.colorRedFailed);
                        Toast.makeText(MainActivity.this, R.string.alert_battery_low, Toast.LENGTH_LONG).show();
                    }
                    break;

                default:
                    throw new IllegalStateException("Unexpected value: " + action);
            }

        }
    }

    private void volumeUpDown() {
        if(mMediaPlayer.isPlaying()) {
            if(mMediaPlayer.getVolume() == 0) {
                volumeButton.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_volume_up_black_24dp));
                mMediaPlayer.setVolume(AUDIO_LEVEL);
            } else {
                volumeButton.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_volume_off_black_24dp));
                mMediaPlayer.setVolume(0);
            }
        } else {
            volumeButton.setImageDrawable(ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_volume_off_black_24dp));
            mMediaPlayer.setVolume(0);
        }
    }


    private static class PlayVideoAsyncTask extends AsyncTask<Void, Void, Void> {

        private WeakReference<Context> mContextRef;

        PlayVideoAsyncTask(MainActivity context) {
            mContextRef = new WeakReference<Context>(context);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            Log.d(TAG, "Video Async: " + STREAMING_URI);

            try {
                final MainActivity context = (MainActivity) mContextRef.get();

                if (context != null && !context.isFinishing()) {
                    context.playButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            context.videoOnOff();
                    /*liveStreamingView.setVideoURI(Uri.parse(liveStreamUrl));
                    liveStreamingView.setMediaController(new MediaController(MainActivity.this));
                    liveStreamingView.requestFocus();
                    liveStreamingView.start();*/
                        }
                    });

                    context.volumeButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            context.volumeUpDown();
                        }
                    });
                }
            } catch (Exception e) {
                Log.d(TAG, "Video ON/OFF async failed");
            }


            return null;
        }
    }

    public void unAuthAction() {
        Log.d(TAG, "Alert dialog creating");
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(MainActivity.this);
        alertBuilder.setCancelable(false);
        Drawable blockIcon = ContextCompat.getDrawable(MainActivity.this, R.drawable.ic_block_black_24dp);
        assert blockIcon != null;
        blockIcon.setTint(ContextCompat.getColor(MainActivity.this, R.color.colorRed));
        alertBuilder.setIcon(blockIcon);
        alertBuilder.setTitle(R.string.msg_unexpected_error)
                .setMessage(R.string.msg_unauth_action)
                .setPositiveButton(R.string.label_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finishAndRemoveTask();
                    }
                }).show();

        /*if(!MainActivity.this.isFinishing()) {
            alertBuilder.show();
        }*/

        //Toast.makeText(MainActivity.this, R.string.msg_unauth_action, Toast.LENGTH_LONG).show();
    }
}
