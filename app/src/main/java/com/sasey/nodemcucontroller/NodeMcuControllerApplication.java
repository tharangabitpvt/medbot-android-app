package com.sasey.nodemcucontroller;

import android.app.Application;

public class NodeMcuControllerApplication extends Application {

    public static final String MYPREFS = "nmcucPrefs";
    public static final String spKeyAdminUsername = "admin_user";
    public static final String spKeyAdminPwd = "admin_pwd";
    public static final String spKeyPwdHash = "pwd_hash";
    public static final String spKeyIPAddress = "ip_address";
    //public static final String spKeyReversePWM = "revers_pwm";
    public static final String spKeyForwardPWM_L = "forward_pwm_l";
    public static final String spKeyForwardPWM_R = "forward_pwm_r";
    public static final String spKeyBackPwmRight = "back_PWM_R";
    public static final String spKeyBackPwmLeft = "back_pwd_l";
    public static final String spKeyRtspUri = "ip_cam_rtsp_uri";
    public static final String spKeyUserPwd = "user_pwd";
    public static final String spKeyAppKey = "app_key";


    // configurations
    public static final String spKeyIsVideoEnabled = "is_video_enabled";

    private static NodeMcuControllerApplication btAppInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        btAppInstance = this;
    }

    public static synchronized NodeMcuControllerApplication getInstance() {
        return btAppInstance;
    }

}
