package com.sasey.nodemcucontroller;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import android.os.Handler;
import android.util.ArrayMap;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class Settings extends AppCompatActivity {
    private static final String DEBUG_TAG = "Settings view";

    EditText ipAddress;
    //SeekBar seekbarFPWM;
    //SeekBar seekbarBPWM;
    EditText forwardPWMLeft;
    EditText forwardPWMRight;
    EditText adminPwd;
    EditText rtspUri;
    EditText backPWMRight;
    EditText backPWMLeft;
    Button rebootButton;

    public String ipAddressValue = "";
    public int fPWM_L_Value = 0;
    public int fPWM_R_Value = 0;
    public int bPWM_L_Value = 0;
    public int bPWM_R_Value = 0;

    SharedPreferences sharedPreferences;

    String mcuIp = "";

    private String APP_KEY;
    private String authKey = "2sMUxEN1xF";

    public static ArrayMap<Integer, Integer> pwdMapper = new ArrayMap<>();
    public static int currentPWDValue = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        sharedPreferences = this.getSharedPreferences(NodeMcuControllerApplication.MYPREFS, Context.MODE_PRIVATE);

        initViews();
    }

    @Override
    protected void onPause() {
        finish();
        super.onPause();
    }

    private void initViews() {
        ipAddress = findViewById(R.id.setting_ip_address);
        //seekbarFPWM = findViewById(R.id.seekBarFPWM);
        //seekbarBPWM = findViewById(R.id.seekBarBPWM);
        forwardPWMLeft = findViewById(R.id.forward_pwd_l);
        forwardPWMRight = findViewById(R.id.forward_pwm_r);
        //textBPWMValue = findViewById(R.id.b_pwm_value);
        adminPwd = findViewById(R.id.setting_pwd);
        rtspUri = findViewById(R.id.rtsp_uri);
        backPWMRight = findViewById(R.id.back_pwd_r);
        backPWMLeft = findViewById(R.id.back_pwd_l);
        rebootButton = findViewById(R.id.btn_reboot);


        ipAddressValue = sharedPreferences.getString(NodeMcuControllerApplication.spKeyIPAddress, getString(R.string.default_ip));
        ipAddress.setText(ipAddressValue);
        fPWM_L_Value = sharedPreferences.getInt(NodeMcuControllerApplication.spKeyForwardPWM_L, 180);
        fPWM_R_Value = sharedPreferences.getInt(NodeMcuControllerApplication.spKeyForwardPWM_R, 180);
        rtspUri.setText(sharedPreferences.getString(NodeMcuControllerApplication.spKeyRtspUri, ""));

        backPWMRight.setText(String.format(Locale.US, "%d", sharedPreferences.getInt(NodeMcuControllerApplication.spKeyBackPwmRight, 170)));
        backPWMLeft.setText(String.format(Locale.US, "%d", sharedPreferences.getInt(NodeMcuControllerApplication.spKeyBackPwmLeft, 170)));
        adminPwd.setText(null);


        forwardPWMLeft.setText(String.format(Locale.US, "%d", fPWM_L_Value));
        forwardPWMRight.setText(String.format(Locale.US, "%d", fPWM_R_Value));

        APP_KEY = sharedPreferences.getString(NodeMcuControllerApplication.spKeyAppKey, "");

        mcuIp = ipAddressValue;

        if(!mcuIp.startsWith("http://")) {
            mcuIp = String.format(Locale.US, "http://%s", mcuIp);
        }

        rebootButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder rebootAlert = new AlertDialog.Builder(Settings.this);
                rebootAlert.setCancelable(false);
                rebootAlert.setTitle(R.string.title_reboot)
                        .setMessage(R.string.message_reboot)
                        .setPositiveButton(R.string.label_reboot, null).setNegativeButton(R.string.label_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                final AlertDialog rebootDialog = rebootAlert.create();

                if(!Settings.this.isFinishing()) {
                    rebootDialog.show();

                    Button positiveButton = rebootDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                    positiveButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final RequestQueue queue = Volley.newRequestQueue(Settings.this);

                            String url = String.format(Locale.US, "%s/reboot", Settings.this.mcuIp);

                            StringRequest stringRequestRebooting = new StringRequest(Request.Method.POST, url,
                                    new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(String response) {
                                            if(response.equals("rebooting")) {
                                                new Thread() {
                                                    int i = 5;
                                                    public void run() {
                                                        while (i-- > 0) {
                                                            try {
                                                                runOnUiThread(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                        rebootAlert.setMessage(String.format(Locale.US, "%s in %d seconds", R.string.msg_connecting, i));
                                                                    }
                                                                });
                                                                Thread.sleep(1000);
                                                            } catch (InterruptedException e) {
                                                                e.printStackTrace();
                                                            }
                                                        }

                                                        rebootAlert.setMessage(R.string.msg_connecting);

                                                        int n = 0;
                                                        while(n++ <= 5) {
                                                            String url2 = String.format(Locale.US, "%s/con_check", Settings.this.mcuIp);
                                                            StringRequest stringRequestConCheck = new StringRequest(Request.Method.GET, url2,
                                                                    new Response.Listener<String>() {
                                                                        @Override
                                                                        public void onResponse(String response) {
                                                                            if(response.equals("con success")) {
                                                                                rebootDialog.dismiss();

                                                                                Intent appInitActivity = new Intent(Settings.this, AppInitActivity.class);
                                                                                startActivity(appInitActivity);
                                                                            }

                                                                            Log.d("VolleyRes", "volley res: " + response);
                                                                        }
                                                                    }, new Response.ErrorListener() {
                                                                @Override
                                                                public void onErrorResponse(VolleyError error) {

                                                                    Log.d("VolleyRes", "volley error: " + error.getMessage());
                                                                    if((error.networkResponse != null) && (error.networkResponse.statusCode == 401)) {
                                                                        unAuthAction();
                                                                    }
                                                                }
                                                            }){
                                                                /** Passing some request headers* */
                                                                @Override
                                                                public Map<String, String> getHeaders() {
                                                                    HashMap<String, String> headers = new HashMap<>();
                                                                    headers.put("MDAuthCheck", authKey);
                                                                    return headers;
                                                                }
                                                            };

                                                            queue.add(stringRequestConCheck);
                                                        }

                                                        // restart app if connection is not present
                                                        Intent appIniti = new Intent(Settings.this, AppInitActivity.class);
                                                        startActivity(appIniti);
                                                        finish();

                                                    }
                                                }.start();
                                            }

                                            Log.d("VolleyRes", "volley res: " + response);
                                        }
                                    }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {

                                    Log.d("VolleyRes", "volley error: " + error.getMessage());
                                    if((error.networkResponse != null) && (error.networkResponse.statusCode == 401)) {
                                        unAuthAction();
                                    }
                                }
                            }){
                                /** Passing some request headers* */
                                @Override
                                public Map<String, String> getHeaders() {
                                    HashMap<String, String> headers = new HashMap<>();
                                    headers.put("MDAuthCheck", authKey);
                                    return headers;
                                }
                            };

                            queue.add(stringRequestRebooting);
                        }
                    });
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.settings_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.save_btn:
                AlertDialog.Builder confirmDialogBuilder = new AlertDialog.Builder(this);

                confirmDialogBuilder.setCancelable(false);
                confirmDialogBuilder.setMessage(R.string.alert_msg_save_settings).setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Settings.this.saveSettings();
                        dialog.dismiss();
                    }
                }).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                if(!Settings.this.isFinishing()) {
                    confirmDialogBuilder.show();
                }

                return true;
//            case R.id.help:
//                showHelp();
//                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void saveSettings() {

        SharedPreferences.Editor spEditor = sharedPreferences.edit();

        int fPWM_L = Integer.parseInt(forwardPWMLeft.getText().toString());
        int fPWM_R = Integer.parseInt(forwardPWMRight.getText().toString());
        int bPWM_R = Integer.parseInt(backPWMRight.getText().toString());
        int bPWM_L = Integer.parseInt(backPWMLeft.getText().toString());

        Log.d(DEBUG_TAG, "fPWM: " + fPWM_L);

        spEditor.putString(NodeMcuControllerApplication.spKeyIPAddress, ipAddress.getText().toString());
        spEditor.putInt(NodeMcuControllerApplication.spKeyForwardPWM_L, fPWM_L);
        spEditor.putInt(NodeMcuControllerApplication.spKeyForwardPWM_R, fPWM_R);
        //spEditor.putInt(NodeMcuControllerApplication.spKeyReversePWM, seekbarBPWM.getProgress());
        spEditor.putString(NodeMcuControllerApplication.spKeyRtspUri, rtspUri.getText().toString());
        spEditor.putInt(NodeMcuControllerApplication.spKeyBackPwmRight, bPWM_R);
        spEditor.putInt(NodeMcuControllerApplication.spKeyBackPwmLeft, bPWM_L);

        if(!adminPwd.getText().toString().isEmpty()) {
            try {
                String hashedPwd = PwdHashing.getHashPwd(adminPwd.getText().toString());
                spEditor.putString(NodeMcuControllerApplication.spKeyAdminPwd, hashedPwd);
            } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
                e.printStackTrace();
            }

        }

        spEditor.apply();

        if(spEditor.commit()){
            sendPWMtoMCU(fPWM_L, fPWM_R, bPWM_L, bPWM_R);
        } else {
            Toast.makeText(Settings.this, R.string.msg_saving_failed, Toast.LENGTH_SHORT).show();
        }
    }

    private void sendPWMtoMCU(int forwardPWM_L, int forwardPWM_R, int backPWML, int backPWMR) {

        final int paramFPWM_L = forwardPWM_L;
        final int paramBPWM_R = forwardPWM_R;
        final int paramBackPWM_L = backPWML;
        final int paramBackPWM_R = backPWMR;

        final boolean[] returnValue = {false};

        final RequestQueue queue = Volley.newRequestQueue(Settings.this);

        mcuIp = ipAddress.getText().toString();

        if(!mcuIp.startsWith("http://")) {
            mcuIp = String.format(Locale.US, "http://%s", mcuIp);
        }

        String url = mcuIp + "/con_check";

        Log.d(DEBUG_TAG, "on Pause called mcuURL: " + url);
        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
            new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if(response.equals("con success")) {
                        String url2 = String.format(Locale.US, "%s/save_settings?f_pwm_l=%d&f_pwm_r=%d&b_pwm_l=%d&b_pwm_r=%d&app_key=%s", Settings.this.mcuIp, paramFPWM_L, paramBPWM_R, paramBackPWM_L, paramBackPWM_R, APP_KEY);

                        StringRequest stringRequestSendPWM = new StringRequest(Request.Method.GET, url2,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        if(response.equals("pwm set success")) {
                                            Intent mainView = new Intent(Settings.this, MainActivity.class);
                                            startActivity(mainView);
                                            Toast.makeText(Settings.this, R.string.msg_settings_saved, Toast.LENGTH_SHORT).show();
                                        } else {
                                            AlertDialog.Builder alertBuilder = new AlertDialog.Builder(Settings.this);
                                            alertBuilder.setTitle(R.string.alert_title_medbot_con_failed);
                                            alertBuilder.setMessage(R.string.msg_pwm_sending_failed);

                                            if(!Settings.this.isFinishing()) {
                                                alertBuilder.show();
                                            }
                                        }

                                        Log.d("VolleyRes", "volley res: " + response);
                                    }
                                }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                                Log.d("VolleyRes", "volley error: " + error.getMessage());
                                if((error.networkResponse != null) && (error.networkResponse.statusCode == 401)) {
                                    unAuthAction();
                                }
                            }
                        }){
                            /** Passing some request headers* */
                            @Override
                            public Map<String, String> getHeaders() {
                                HashMap<String, String> headers = new HashMap<>();
                                headers.put("MDAuthCheck", authKey);
                                return headers;
                            }
                        };

                        queue.add(stringRequestSendPWM);
                    } else {
                        Toast.makeText(Settings.this, R.string.msg_unexpected_error, Toast.LENGTH_SHORT).show();
                    }

                    Log.d("VolleyRes", "volley res: " + response);
                }
            }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
                Log.d("VolleyRes", "volley error: " + error.getMessage());
                if((error.networkResponse != null) && (error.networkResponse.statusCode == 401)) {
                    unAuthAction();
                }

                Toast.makeText(Settings.this, R.string.msg_pwm_sending_failed, Toast.LENGTH_SHORT).show();
            }
        }){
            /** Passing some request headers* */
            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("MDAuthCheck", authKey);
                return headers;
            }
        };

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public void unAuthAction() {
        Log.d("Settings page", "Alert dialog creating");
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(Settings.this);
        alertBuilder.setCancelable(false);
        Drawable blockIcon = ContextCompat.getDrawable(Settings.this, R.drawable.ic_block_black_24dp);
        assert blockIcon != null;
        blockIcon.setTint(ContextCompat.getColor(Settings.this, R.color.colorRed));
        alertBuilder.setIcon(blockIcon);
        alertBuilder.setTitle(R.string.msg_unexpected_error)
                .setMessage(R.string.msg_unauth_action)
                .setPositiveButton(R.string.label_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finishAndRemoveTask();
                    }
                }).show();

        /*if(!MainActivity.this.isFinishing()) {
            alertBuilder.show();
        }*/

        //Toast.makeText(MainActivity.this, R.string.msg_unauth_action, Toast.LENGTH_LONG).show();
    }

}
